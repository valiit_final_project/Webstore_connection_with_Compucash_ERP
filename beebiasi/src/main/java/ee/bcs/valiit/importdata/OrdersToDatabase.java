package ee.bcs.valiit.importdata;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class OrdersToDatabase {

	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/beebiasi";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "tere";

	public static String clearTable() {
		String info = "";
		String sql = "delete from beebiasi.order_item";
		info += performSqlUpdate(sql);
		sql = "delete from beebiasi.order";
		info += performSqlUpdate(sql);
		return info;
	}

	public static String saveOrders(List<Order> orderList) {
		String info = "";
		for (Order order : orderList) {
			String sql = "INSERT INTO beebiasi.order (id, amount, info)";
			sql = sql + String.format(" values ('%s','%s','%s')", order.getDocumentNum(), order.getAmount(),
					order.getInfo());
			info += performSqlUpdate(sql);
		}
		return info;
	}

	public static String saveOrderItems(List<OrderItem> orderItemsList) {
		String info = "";
		for (OrderItem orderItem : orderItemsList) {
			String sql = "INSERT INTO beebiasi.order_item (id, barcode, description, order_id, quantity, price)";
			sql = sql + String.format(" values ('%s','%s','%s','%s','%s','%s')", orderItem.getOrderItemId(),
					orderItem.getBarcode(), orderItem.getDescription().replaceAll("'", ""), orderItem.getFkOrderId(),
					orderItem.getQuantity(), orderItem.getSalePrice());
			info += performSqlUpdate(sql);
		}
		return info;
	}

	public static synchronized String performSqlUpdate(String sql) {
		Connection conn = null;
		Statement stmt = null;
		String info = "";
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
			conn.setAutoCommit(true);
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException | ClassNotFoundException e) {
			info += e.toString();
		} finally {
			if (conn != null) {
				try {
					stmt.close();
					conn.close();
				} catch (SQLException e) {
					info += "| |" + e.toString();
				}
			}
		}
		return info;
	}
}
