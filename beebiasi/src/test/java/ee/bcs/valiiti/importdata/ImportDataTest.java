package ee.bcs.valiiti.importdata;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.valiit.importdata.ImportData;

public class ImportDataTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDownloadFile() throws IOException {
		String response = ImportData.downloadFile("https://www.beebiasi.ee/et/prod.xlsx",
				".");
		File f = new File("./order_information.xlsx");
		assertTrue(f.exists());
		assertTrue(response.equals("File downloaded!\r\n "));
		}

}
