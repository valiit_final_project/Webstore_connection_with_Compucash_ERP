package ee.bcs.valiit.rest;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import ee.bcs.valiit.importdata.ImportData;
import ee.bcs.valiit.importdata.Login;
import ee.bcs.valiit.importdata.Order;
import ee.bcs.valiit.importdata.OrdersFromDatabase;
import ee.bcs.valiit.importdata.ReadFromExcel;

@Path("/")
public class RestResource {

	@GET
	@Path("/importfile")
	@Produces(MediaType.TEXT_PLAIN)
	public String importFile(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("IS_AUTHENTICATED") != null
				&& session.getAttribute("IS_AUTHENTICATED").equals("TRUE")) {
			try {
				String info = ImportData.downloadFile("https://www.beebiasi.ee/et/prod.xlsx",
						".");
				return info;
			} catch (IOException e) {
				return "File download failed. More information: " + e.toString();
			}
		} else {
			return "Not authenticated! Please log in.";
		}
	}

	@GET
	@Path("/readexcel")
	@Produces(MediaType.TEXT_PLAIN)
	public String readExcel(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("IS_AUTHENTICATED") != null
				&& session.getAttribute("IS_AUTHENTICATED").equals("TRUE")) {
			
			String info = ReadFromExcel.readFromExcel(".\\order_information.xlsx");
			return info;

		} else {

			return "";
		}
	}

	@POST
	@Path("/login")
	@Produces(MediaType.TEXT_PLAIN)

	public String login(@Context HttpServletRequest req, @FormParam("username") String username,
			@FormParam("password") String password) {
		String loginInfo = Login.login(username, password);
		if (loginInfo.equals("Welcome!")) {
			HttpSession session = req.getSession(true);
			session.setAttribute("IS_AUTHENTICATED", "TRUE");
		}
		return loginInfo;

	}

	@GET
	@Path("/logout")

	public void logout(@Context HttpServletRequest req) {

		HttpSession session = req.getSession(true);
		session.setAttribute("IS_AUTHENTICATED", "FALSE");

	}

	@GET
	@Path("/lastImportedOrders")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Order> getDBOrderList()  {

		try {
			return OrdersFromDatabase.getDBOrderList();
		} catch (ClassNotFoundException e) {
	
			e.printStackTrace();
			return null;
		} catch (SQLException e) {

			e.printStackTrace();
			return null;
		}

	}



}