package ee.bcs.valiit.importdata;

public class Token {
	private String access_token;
	private String token_type;

	public String getAccess_token() {
		return access_token;
	}

	public String getToken_type() {
		return token_type;
	}

}
