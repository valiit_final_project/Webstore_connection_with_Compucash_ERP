package ee.bcs.valiit.importdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.json.XML;

import com.google.gson.Gson;

public class ReadSmartPostInfo {

	public static String createOrderXML(String postType) {

		Gson gson = new Gson();
		SmartPostOrder smartPostOrder = readSmartPostInfo(".\\order_information.xlsx", postType);
		String xml = "";
		if (!smartPostOrder.getItemsList().isEmpty()) {
			String json = gson.toJson(smartPostOrder);
			JSONObject jsonObject = new JSONObject(json);
			xml = "<orders>" + XML.toString(jsonObject).replaceAll("placeId", "place_id") + "</orders>";
		}
		System.out.println(xml);
		return xml;

	}

	public static SmartPostOrder readSmartPostInfo(String filePath, String postType) {

		Map<String, String[]> smartPostMap = createPostMap(filePath, postType);
		List<SmartPostItem> smartPostItemList = new ArrayList<SmartPostItem>();
		smartPostItemList = createPostItemList(smartPostMap, postType);
		SmartPostOrder smartPostOrder = new SmartPostOrder();
		smartPostOrder = createSmartPostOrder(smartPostItemList);
		return smartPostOrder;

	}

	public static Map<String, String[]> createPostMap(String filePath, String postType) {
		Map<String, String[]> smartPostMap = new HashMap<String, String[]>();

		Workbook workbook = null;
		FileInputStream excelFile = null;
		try {
			excelFile = new FileInputStream(new File(filePath));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		try {
			workbook = new XSSFWorkbook(excelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sheet datatypeSheet = workbook.getSheetAt(0);
		Iterator<Row> iterator = datatypeSheet.iterator();

		while (iterator.hasNext()) {
			Row currentRow = iterator.next();
			if (currentRow != datatypeSheet.getRow(0)) {
				if (currentRow.getCell(7) != null || currentRow.getCell(8) != null || currentRow.getCell(9) != null) {
					String key = String.valueOf((int) currentRow.getCell(0).getNumericCellValue());
					String name = currentRow.getCell(10).getStringCellValue().split("<div class=\"name-block\">")[1]
							.split("</div>")[0];
					String phone = currentRow.getCell(10).getStringCellValue().split(
							"Telefoni number: </div><div class=\"field-items\"><div class=\"field-item even\">")[1]
									.split("</div>")[0];
					String email = currentRow.getCell(12).getStringCellValue();
					if (currentRow.getCell(7) != null && postType.equals("eeBox")) {
						String boxName = currentRow.getCell(7).getStringCellValue().split("</label> ")[1]
								.split("<br />")[0];
						String[] recipientInfo = { name, phone, email, boxName };
						smartPostMap.put(key, recipientInfo);
					} else if (currentRow.getCell(9) != null && postType.equals("fiBox")) {
						String fiBoxName = currentRow.getCell(9).getStringCellValue().split("</label> ")[1]
								.split("<br />")[0];
						String[] recipientInfo = { name, phone, email, fiBoxName };
						smartPostMap.put(key, recipientInfo);
					} else if (currentRow.getCell(8) != null && postType.equals("kuller")) {
						String street = currentRow.getCell(10).getStringCellValue()
								.split("<div class=\"street-block\"><div class=\"thoroughfare\">")[1]
										.split("</div>")[0];
						String city = currentRow.getCell(10).getStringCellValue()
								.split("</span> <span class=\"locality\">")[1].split("</span>")[0];
						String postalcode = currentRow.getCell(10).getStringCellValue()
								.split("<span class=\"postal-code\">")[1].split("</span>")[0];
						String strTimeWindow = currentRow.getCell(11).getStringCellValue();
						String timeWindow;
						if (strTimeWindow.equals("9-17"))
							timeWindow = "2";
						else if (strTimeWindow.equals("Igal ajal"))
							timeWindow = "1";
						else
							timeWindow = "3";
						String[] recipientInfo = { name, phone, email, street, city, postalcode, timeWindow };
						smartPostMap.put(key, recipientInfo);
					}
				}
			}
		}
		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return smartPostMap;
	}

	private static List<SmartPostItem> createPostItemList(Map<String, String[]> smartPostMap, String postType) {
		List<SmartPostItem> smartPostItemList = new ArrayList<SmartPostItem>();
		for (String key : smartPostMap.keySet()) {
			SmartPostRecipient recipient = new SmartPostRecipient();
			recipient.setEmail(smartPostMap.get(key)[2]);
			recipient.setName(smartPostMap.get(key)[0]);
			recipient.setPhone(smartPostMap.get(key)[1]);
			SmartPostDestination destination = new SmartPostDestination();
			String reference = key;
			SmartPostItem smartPostItem = new SmartPostItem();
			smartPostItem.setReference(reference);

			if (postType.equals("eeBox")) {
				destination.setPlaceId(smartPostMap.get(key)[3]);
			} else if (postType.equals("fiBox")) {
				destination.setRoutingcode(smartPostMap.get(key)[3]);
				String postalcode = destination.getFIpostalCode(smartPostMap.get(key)[3]);
				destination.setPostalcode(postalcode);
			} else if (postType.equals("kuller")) {
				destination.setPostalcode(smartPostMap.get(key)[5]);
				destination.setStreet(smartPostMap.get(key)[3]);
				destination.setCity(smartPostMap.get(key)[4]);
				destination.setTimewindow(Integer.parseInt(smartPostMap.get(key)[6]));
			}

			smartPostItem.setRecipient(recipient);
			smartPostItem.setDestination(destination);
			smartPostItemList.add(smartPostItem);
		}

		return smartPostItemList;
	}

	private static SmartPostOrder createSmartPostOrder(List<SmartPostItem> smartPostItemList) {
		SmartPostAuthentication authentication = new SmartPostAuthentication();
		SmartPostReport report = new SmartPostReport();
		SmartPostOrder smartPostOrder = new SmartPostOrder();
		smartPostOrder.setAuthentication(authentication);
		smartPostOrder.setReport(report);
		smartPostOrder.setItemsList(smartPostItemList);
		return smartPostOrder;
	}

}
