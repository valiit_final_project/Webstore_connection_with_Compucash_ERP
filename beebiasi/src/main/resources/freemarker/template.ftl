<!DOCTYPE html>
<html lang="en">
<head>
<title>Beebiasi webapp</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<link rel="icon" type="image/png" href="/beebiasi/pacifier.ico">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
/* Remove the navbar's default margin-bottom and rounded borders */
.navbar {
	margin-bottom: 0;
	border-radius: 0;
}

/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
.row.content {
	height: 86vh
}

/* Set gray background color and 100% height */
.sidenav {
	padding-top: 20px;
	background-color: #f1f1f1;
	height: 100%;
}

/* Set black background color, white text and some padding */
footer {
	background-color: #555;
	color: white;
	padding: 15px;
	bottom: 0px;
}

/* On small screens, set height to 'auto' for sidenav and grid */
@media screen and (max-width: 767px) {
	.sidenav {
		height: auto;
		padding: 0px;
	}
	.row.content {
		height: 100vh;
	}
	body {
		background: none;
	}
}

#customers {
	border-collapse: collapse;
	width: 100%;
}

#customers td, #customers th {
	border: 1px solid #ddd;
	padding: 8px;
}

#customers tr:nth-child(odd) {
	background-color: #f2f2f2;
}

#customers tr:nth-child(even) {
	background-color: white;
}

#customers tr:hover {
	background-color: #ddd;
}

#customers th {
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #f2f2f2;;
	border: 1px solid #ddd;
}
</style>

</head>
<body background="/beebiasi/blueWave.jpg"
	style="background-size: contain; background-repeat: no-repeat; background-position: bottom">
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<#if authStatus=="FALSE">
					<li<#if page=="home"> class="active"</#if>><a href="home">Home</a></li>
					<li><a href="javascript:openLoginModal('lastOrders')"
						id="login" class="showLogin">Last Orders</a></li>
					<li<#if page=="contact"> class="active" </#if> ><a
						href="contact">Contact</a></li>
					</#if>

					<#if authStatus=="TRUE">
					<li<#if page=="sendOrders"> class="active" </#if> ><a
						href=sendOrders>Send Orders</a></li>
					<li<#if page=="lastOrders"> class="active" </#if>><a
						href="lastOrders">Last Orders</a></li>
					<li<#if page=="contact"> class="active" </#if> ><a
						href="contact">Contact</a></li>
					</#if>

				</ul>
				<ul class="nav navbar-nav navbar-right">
					<#if authStatus=="FALSE">
					<li><a id="login" class="showLogin"
						href="javascript:openLoginModal('${page}')"> <span
							class="glyphicon glyphicon-log-in"></span> <span>Login</a></li>
					</#if>
					<#if authStatus=="TRUE">
					<li><a id="logout" class="showLogout"
						href="javascript:logout()"> <span
							class="glyphicon glyphicon-log-out"></span> <span>Logout</a></li>
					</#if>

				</ul>
			</div>
		</div>
	</nav>

	<div class="container-fluid text-center">
		<div class="row content">
			<div class="col-sm-2 sidenav">
				<p>
					<a href="#"></a>
				</p>

			</div>
			<div class="col-sm-8 text-left ">

				<hr>

				<#if page=='sendOrders'>
				<h1>
					</br> </br> </br>Beebiasi Webapp
				</h1>
				</#if>

				<#if page=='home'>
				<h1>
					</br> </br> </br>Welcome to Beebiasi Webapp!
				</h1>

				<p style="font-size: 120%">This application is made as final
					project of Choose-IT program by Ilja Kulikov and Maria Atonen
					between 2018-03-09 and 2018-03-29. Using this app you can import
					orders from beebiasi.ee web-store, send them to the CompuCash ERP
					and export delivery information to SmartPost. You can also check
					information about last added orders. To use full functionality of
					the site please login.</p>
				</#if>

				<#if page=='sendOrders'>
				<button type="button" class="btn btn-primary btn-lg" id="alertbox"
					onclick="sendOrders()"
					style="background-color: #0086b3; border-style: none; border-radius: 0px;">Send
					orders</button>
				</#if>

				<#if page=='contact'>
				<h1 style="font-size: 3.5em; color: #0086b3">
					Contact</br>
				</h1>

				<div class="media">
					<div class="media-left media-middle hidden-xs">
						<a href="#"> <img class="media-object"
							src="/beebiasi/iljaAvatar.png" style="height: 20vh"
							alt="Ilja Kulikov avatar">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading" style="font-size: 2em;">Ilja
							Kulikov</h4>
						<p style="font-size: 1.2em;">
							e-mail: ilja.kulikov@mail.ee</br> telefon:+372 56 687 833</br>
						</p>
						<a href="https://www.linkedin.com/in/ilja-kulikov-2384b143"> <img
							src="/beebiasi/linkedin.png" alt="Linkedin icon"
							style="width: 30px;">
					</div>
				</div>
				<p>
					</br>
				</p>
				<div class="media">
					<div class="media-left media-middle hidden-xs">
						<a href="#"> <img class="media-object"
							src="/beebiasi/mariaAvatar.png" alt="Maria Atonen avatar"
							style="height: 20vh">
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading" style="font-size: 2em;">Maria
							Atonen</h4>
						<p style="font-size: 1.2em;">
							e-mail: maria.atonen@gmail.com</br> telefon:+372 58 293 535</br>
						</p>
						<a href="https://www.linkedin.com/in/maria-atonen"> <img
							src="/beebiasi/linkedin.png" alt="Linkedin icon"
							style="width: 30px;">
					</div>

				</div>
				</#if>


				<#if page=='lastOrders'>

				<h1>
					</br> Last imported orders:

				</h1>
				<div id="orders_list"></div>

				</#if>
			</div>
			<div class="col-sm-2 sidenav">
				<div>
					<p></p>
				</div>
				<div>
					<p></p>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog" style="padding-top: 10%;">

			<!-- Modal content-->
			<div class="modal-content" style="border-radius: 0px;">
				<div class="modal-header" style="background-color: #0086b3;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="color: white;">Information</h4>
				</div>
				<div class="modal-body">
					<p id="error"></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"
						style="background-color: #0086b3; color: white; border-radius: 0px;">Close</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Login Modal -->
	<div class="modal fade" id="loginModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document" style="padding-top: 10%;">
			<div class="modal-content" style="border-radius: 0px;">
				<div class="modal-header" style="background-color: #0086b3;">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="text" class="form-control" id="Username"
						placeholder="Username"><br> <input type="password"
						class="form-control" id="Password" placeholder="Password"><br>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"
						style="background-color: #0086b3; color: white; border-radius: 0px;">Close</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"
						style="background-color: #0086b3; color: white; border-radius: 0px;"
						onclick="login()">Login</button>
				</div>
			</div>
		</div>
	</div>


	<script>
		var successfulRedirectUrl = "";

		<#if page == 'sendOrders'>
		var info;
		function sendOrders() {

			$.get("/beebiasi/rest/importfile", function(result) {
				info = result;
				$.get("/beebiasi/rest/readexcel", function(result) {
					$('#myModal').modal("show");
					$("#error").html(info + "<br />" + result);

				});

			});
		}

		</#if>

		function login() {

			$.ajax({
				url : "/beebiasi/rest/login",
				type : "post",

				data : {
					"username" : $("#Username").val(),
					"password" : $("#Password").val()

				},
				complete : function(result) {
					$('#myModal').modal("show");
					$("#error").html(result.responseText);

					if (result.responseText == "Welcome!") {
						if (successfulRedirectUrl != "") {
							if (successfulRedirectUrl == "home") {
								successfulRedirectUrl = "sendOrders";
							}
							window.location = successfulRedirectUrl;
						}

					}
				}
			});

		}

		function logout() {
			$.get("/beebiasi/rest/logout", function(result) {
				<#if page == 'sendOrders' || page == 'lastOrders' >
				window.location = 'home';
				<#else>
				window.location = 'contact';
				</#if>
			});
		}

		function openLoginModal(successUrl) {
			successfulRedirectUrl = successUrl;
			$('#loginModal').modal('show');
		}

		<#if page ==  'lastOrders'>

		$(document).ready(function() {
			getOrdersFromDB();

		});

		function getOrdersFromDB() {
			$
					.get(
							"/beebiasi/rest/lastImportedOrders",
							function(result) {
								$("#orders_list").empty();

								for (var i = 0; i < result.length; i++) {
									var orderBody = '<tr><th>Barcode </th> <th>Description</th> <th>Quantity</th> <th> Sum </th></tr>';

									for (var j = 0; j < result[i].orderListOfOrderItems.length; j++) {

										orderBody += '<tr><td>'
												+ result[i].orderListOfOrderItems[j].barcode
												+ '</td> <td>'
												+ result[i].orderListOfOrderItems[j].description
												+ '</td> <td>'
												+ result[i].orderListOfOrderItems[j].quantity
												+ ' pc </td> <td>'
												+ result[i].orderListOfOrderItems[j].salePrice
												* result[i].orderListOfOrderItems[j].quantity
												+ ' EUR </td></tr>';

									}

									$('#orders_list').append(
											'<div class="card" >');
									$('#orders_list')
											.append(
													'<div style="background-color: #0086b3; border-radius: 0px;" class="card-header" id="heading'+i+'"><h5 class="mb-0"><button class="btn " style="background-color: #0086b3; border-radius: 0px; color: white; font-size: 120%"  data-toggle="collapse" data-target="#collapse'
															+ i
															+ '" aria-expanded="true"aria-controls="collapse'
															+ i
															+ '"> '
															+ "Order ID: "
															+ result[i].documentNum
															+ " Total amount: "
															+ result[i].amount
															+ " EUR </button> </h5> </div>");
									$('#orders_list')
											.append(
													'<div id="collapse'+i+'" class="collapse" aria-labelledby="heading'+i+'" data-parent="#orders_list"><div class="card-body"><table id="customers">'
															+ orderBody
															+ ' </div></div>');
									$('#orders_list').append('</div>');

								}

							});

		}

		</#if>
	</script>
	<footer class="container-fluid text-center">
		<p>
			&copy;
			<script type="text/javascript">
				document.write(new Date().getFullYear());
			</script>
			Beebiasi OÜ
		</p>
	</footer>

</body>
</html>




