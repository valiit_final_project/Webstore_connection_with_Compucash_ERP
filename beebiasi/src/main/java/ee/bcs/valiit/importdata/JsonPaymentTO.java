package ee.bcs.valiit.importdata;

public class JsonPaymentTO {

	public final int PAYMENTTYPE = 12;// variable name is written together to match compuCash JSON format
	public String amount;
	public String paymentTime;
	public final String CURRENCY = "EUR";

	public JsonPaymentTO(Order order) {
		this.amount = String.valueOf(order.getAmount());
		this.paymentTime = order.getPaymentTime();
	}


}
