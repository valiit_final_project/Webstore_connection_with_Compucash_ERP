package ee.bcs.valiit.importdata;

public class JsonOrderItemTO {

	public String barcode;
	public String quantity;
	public String salePrice;

	public JsonOrderItemTO(OrderItem orderItem) {
		this.barcode = String.valueOf(orderItem.getBarcode());
		this.quantity = String.valueOf(orderItem.getQuantity());
		this.salePrice = String.valueOf(orderItem.getSalePrice());
	}


}