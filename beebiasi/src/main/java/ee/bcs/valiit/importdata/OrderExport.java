package ee.bcs.valiit.importdata;

import java.util.List;
import com.google.gson.Gson;

public class OrderExport {

	public static String getJson(Order order) {

		List<OrderItem> orderItemsList = ReadFromExcel.getOrderItemsList();
		String jsonExport = "";

		jsonExport = " {\"Document\":{\"DocumentType\":\"ORDER\",\"payments\":{\"payment\":[";

		Gson gsonPayment = new Gson();
		JsonPaymentTO jsonPayment = new JsonPaymentTO(order);
		String jsonPaymentString = gsonPayment.toJson(jsonPayment);
		jsonExport += jsonPaymentString;

		jsonExport += "]},\"Buyer\":{\"Name\":\"E-pood\",\"BusinessCode\":\"111\"},\"DocumentInfo\":";

		Gson gsonDocumentInfo = new Gson();
		JsonDocumentInfoTO jsonDocumentInfo = new JsonDocumentInfoTO(order);
		String jsonDocumentInfoString = gsonDocumentInfo.toJson(jsonDocumentInfo);
		jsonExport += jsonDocumentInfoString;

		jsonExport += ",\"DocumentItems\":{\"RowItem\":[";

		for (OrderItem orderItem : orderItemsList) {
			if (orderItem.getFkOrderId() == order.getDocumentNum()) {
				Gson gsonOrderItem = new Gson();
				JsonOrderItemTO jsonOrderItem = new JsonOrderItemTO(orderItem);
				String jsonOrderItemString = gsonOrderItem.toJson(jsonOrderItem);
				jsonExport += jsonOrderItemString;
				jsonExport += ",";
			}

		}
		jsonExport = jsonExport.substring(0, jsonExport.length() - 1);// removes last comma
		jsonExport += "]}}}";

		return jsonExport;
	}

}
