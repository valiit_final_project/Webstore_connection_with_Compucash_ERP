package ee.bcs.valiit.importdata;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SmartPostDestination {

	private String postalcode;
	private String routingcode;
	private String street;
	private String city;
	private final String country = "Estonia"; // used lower case to match xml format
	private int timewindow;

	private String placeId;

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getRoutingcode() {
		return routingcode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getTimewindow() {
		return timewindow;
	}

	public void setTimewindow(int timewindow) {
		this.timewindow = timewindow;
	}

	public String getCountry() {
		return country;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId.toString();
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String boxName) {
		String sql = "select place_id from beebiasi.smartpost_terminals where name ='" + boxName + "';";
		ResultSet result = null;
		try {
			result = OrdersFromDatabase.performSqlSelect(sql);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			result.next();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			this.placeId = result.getString(1);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void setRoutingcode(String fiBoxName) {
		String sql = "select routingcode from beebiasi.smartpost_fi_terminals where name ='" + fiBoxName + "';";
		ResultSet result = null;
		try {
			result = OrdersFromDatabase.performSqlSelect(sql);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			result.next();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			this.routingcode = result.getString(1);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public String getFIpostalCode(String fiBoxName){
		String sql = "select postalcode from beebiasi.smartpost_fi_terminals where name ='" + fiBoxName + "';";
		ResultSet result = null;
		try {
			result = OrdersFromDatabase.performSqlSelect(sql);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		try {
			result.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			return result.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}
}