package ee.bcs.valiit.importdata;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {

	static List<Order> orderList = new ArrayList<>();
	static List<OrderItem> orderItemsList = new ArrayList<>();

	public static String readFromExcel(String filePath) {

		Map<Integer, Float> orderMap = new HashMap<Integer, Float>();
		Map<Integer, String> deliveryMap = new HashMap<Integer, String>();
		Map<Integer, String> selfPickUpMap = new HashMap<Integer, String>();
		String info = "";

		info += createOrderItemsList(filePath, orderMap, deliveryMap, selfPickUpMap) + "<br />";
		createOrderList(orderMap);		
		info += isListEmpty(orderItemsList);
		addDeliveryInformation(deliveryMap);
		addSelfPickUpInformation(selfPickUpMap);
		if (!orderItemsList.isEmpty()) info += OrdersToDatabase.clearTable(); 
		info += OrdersToDatabase.saveOrders(orderList);
		info += OrdersToDatabase.saveOrderItems(orderItemsList);
		try {
			info += PostRequest.sendPostRequest();
		} catch (ClientProtocolException e) {
			info += e.toString();
		} catch (IOException e) {
			info += e.toString();
		}
		return info;
	}

	@SuppressWarnings("deprecation")
	public static String createOrderItemsList(String filePath, Map<Integer, Float> orderMap,
			Map<Integer, String> deliveryMap, Map<Integer, String> selfPickUpMap) {
		String readFromExcelExceptions = "";
		try {
			FileInputStream excelFile = new FileInputStream(new File(filePath));
			Workbook workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();

			orderItemsList.clear();
			while (iterator.hasNext()) {
				Row currentRow = iterator.next();

				if (currentRow != datatypeSheet.getRow(0)) {
					float rowAmout = Float.valueOf(
							((currentRow.getCell(5).getStringCellValue()).replaceAll(" €", "").replaceAll(",", ".")));
					float orderAmount = Float.valueOf(
							((currentRow.getCell(6).getStringCellValue()).replaceAll(" €", "").replaceAll(",", ".")));
					orderMap.put((int) ((currentRow.getCell(0).getNumericCellValue())), orderAmount);
					OrderItem orderItem = new OrderItem();
					orderItem.setDescription((currentRow.getCell(2).getStringCellValue()));
					orderItem.setQuantity((int) (currentRow.getCell(4).getNumericCellValue()));
					orderItem.setSalePrice((float) Math.round((rowAmout / orderItem.getQuantity()) * 100) / 100);

					if (currentRow.getCell(1).getCellTypeEnum() != CellType.STRING) {
						orderItem.setBarcode(BigInteger.valueOf((long) currentRow.getCell(1).getNumericCellValue()));
					} else {
						if (currentRow.getCell(1).getStringCellValue().contains("-")) {
							orderItem.setBarcode(BigInteger
									.valueOf(Long.parseLong(currentRow.getCell(1).getStringCellValue().split("-")[0])));
							orderItem.setOrderItemInfo(orderItem.getDescription());
						} else {
							int orderItemQuantityMultiPack = Integer
									.valueOf(currentRow.getCell(1).getStringCellValue().split("x")[1]);

							orderItem.setBarcode(BigInteger
									.valueOf(Long.parseLong(currentRow.getCell(1).getStringCellValue().split("x")[0])));
							orderItem.setQuantity(orderItem.getQuantity() * orderItemQuantityMultiPack);
							orderItem.setSalePrice(orderItem.getSalePrice() / orderItemQuantityMultiPack);
							orderItem.setDescription(orderItem.getDescription().split("x")[0]);
						}
					}

					if (currentRow.getCell(7) != null || currentRow.getCell(8) != null
							|| currentRow.getCell(9) != null) {
						deliveryMap.put((int) ((currentRow.getCell(0).getNumericCellValue())), "Delivery is needed");
					} else {
						selfPickUpMap.put((int) ((currentRow.getCell(0).getNumericCellValue())), "Self pickup");
					}

					orderItem.setFkOrderId((int) (currentRow.getCell(0).getNumericCellValue()));
					orderItem.setOrderItemId(Integer.toString(orderItem.getFkOrderId()) + orderItem.getBarcode());
					orderItemsList.add(orderItem);
				}

				orderItemsList = checkBarcode(orderItemsList); // check list to eliminate orderItems with identical
																// barcode.

			}
			readFromExcelExceptions += "Excel reading successful!";
			workbook.close();

		} catch (

		FileNotFoundException e) {
			readFromExcelExceptions += e.toString();
		} catch (IOException e) {
			readFromExcelExceptions += "| |" + e.toString();
		}

		return readFromExcelExceptions;
	}

	public static void addSelfPickUpInformation(Map<Integer, String> selfPickUpMap) {
		for (int orderId : selfPickUpMap.keySet()) {
			for (Order oneOrder : orderList) {
				if (oneOrder.getDocumentNum() == orderId) {
					String info = oneOrder.getInfo() + "; Klient tuleb ise järele; ";
					oneOrder.setInfo(info);
				}
			}
		}
	}

	public static void addDeliveryInformation(Map<Integer, String> deliveryMap) {

		float deliveryPrice = 0;

		for (Integer orderId : deliveryMap.keySet()) {
			OrderItem deliveryOrderItem = new OrderItem();
			deliveryOrderItem.setBarcode(BigInteger.valueOf(999));
			deliveryOrderItem.setDescription("SmartPost");
			deliveryOrderItem.setQuantity(1);
			deliveryOrderItem.setFkOrderId(orderId);
			deliveryOrderItem.setOrderItemId(orderId.toString() + "999");
			orderItemsList.add(deliveryOrderItem);

			for (Order oneOrder : orderList) {
				if (oneOrder.getDocumentNum() == orderId) {
					deliveryPrice = (float) Math
							.round((oneOrder.getAmount() - oneOrder.getAmountWithoutDelivery()) * 100) / 100;
				}
			}

			deliveryOrderItem.setSalePrice(deliveryPrice);

		}
	}

	public static void createOrderList(Map<Integer, Float> orderMap) {
		orderList.clear();

		for (int key : orderMap.keySet()) {
			Order order = new Order();
			order.setDocumentNum(key);
			order.setAmount(orderMap.get(key));

			String info = String.valueOf(order.getDocumentNum()) + "; ";
			float amountWithoutDelivery = 0;
			for (OrderItem orderItem : orderItemsList) {

				if (orderItem.getFkOrderId() == order.getDocumentNum()) {
					amountWithoutDelivery += (orderItem.getSalePrice() * orderItem.getQuantity());

					if (orderItem.getOrderItemInfo() != null) {
						info += orderItem.getOrderItemInfo() + "; ";
					}
				}
				order.setAmountWithoutDelivery(amountWithoutDelivery);
			}
			order.setInfo(info);
			orderList.add(order);
		}
	}

	public static List<Order> getOrderList() {
		return orderList;
	}

	public static List<OrderItem> getOrderItemsList() {
		return orderItemsList;
	}

	public static String isListEmpty(List<OrderItem> orderItemsList) {
		String info = "";
		if (orderItemsList.isEmpty()) {
			info = "No orders to export!";
		}
		return info;
	}

	public static List<OrderItem> checkBarcode(List<OrderItem> orderItemList) {

		Map<String, OrderItem> barcodeMap = new HashMap<String, OrderItem>();

		for (OrderItem orderItem : orderItemList) {

			if (barcodeMap.get(orderItem.getOrderItemId()) != null) {
				int newQuantity = orderItem.getQuantity() + barcodeMap.get(orderItem.getOrderItemId()).getQuantity();
				String newOrderItemInfo = orderItem.getOrderItemInfo()
						+ barcodeMap.get(orderItem.getOrderItemId()).getOrderItemInfo();
				barcodeMap.get(orderItem.getOrderItemId()).setQuantity(newQuantity);
				barcodeMap.get(orderItem.getOrderItemId()).setOrderItemInfo(newOrderItemInfo);
			} else {
			barcodeMap.put(orderItem.getOrderItemId(), orderItem);
			}
		}
		
		return new ArrayList<OrderItem>(barcodeMap.values());
		

	}

}
