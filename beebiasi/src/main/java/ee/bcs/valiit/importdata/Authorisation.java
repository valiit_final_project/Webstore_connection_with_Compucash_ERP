package ee.bcs.valiit.importdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import com.google.gson.Gson;

public class Authorisation {

	public static String getBearer() throws ClientProtocolException, IOException {

		String url = "https://www.compucash5.com/IdentityServer/connect/token";
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> keyList = new ArrayList<NameValuePair>();
		keyList.add(new BasicNameValuePair("grant_type", "client_credentials"));
		keyList.add(new BasicNameValuePair("client_id", "beebiasiweb"));
		keyList.add(new BasicNameValuePair("scope", "cc5api"));
		keyList.add(new BasicNameValuePair("client_secret", "DrU$rageWRES6Spafu+w"));

		StringEntity entity = new UrlEncodedFormEntity(keyList);
		post.setEntity(entity);
		HttpResponse response = client.execute(post);
		
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		Gson gson = new Gson();
		Token token = gson.fromJson(result.toString(), Token.class);
		
		String bearer = token.getToken_type() + " " + token.getAccess_token();
		return bearer;
	}

}
