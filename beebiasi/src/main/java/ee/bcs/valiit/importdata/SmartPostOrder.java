package ee.bcs.valiit.importdata;

import java.util.ArrayList;
import java.util.List;

public class SmartPostOrder {
	
	private SmartPostAuthentication authentication;
	private List<SmartPostItem> item  = new ArrayList<SmartPostItem>();
	private SmartPostReport report;
	
	public SmartPostAuthentication getAuthentication() {
		return authentication;
	}
	public void setAuthentication(SmartPostAuthentication authentication) {
		this.authentication = authentication;
	}
	public List<SmartPostItem> getItemsList() {
		return item;
	}
	public void setItemsList(List<SmartPostItem> itemsList) {
		this.item = itemsList;
	}
	public SmartPostReport getReport() {
		return report;
	}
	public void setReport(SmartPostReport report) {
		this.report = report;
	}

		
}
