package ee.bcs.valiit.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

@Path("/")
public class WebResource {

	@GET
	@Path("/home")
	@Produces(MediaType.TEXT_HTML)

	public Viewable getHome(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		Map<String, String> map = new HashMap<>();
		map.put("page", "home");
		map.put("authStatus", "FALSE");
		return new Viewable("/template.ftl", map);
	}

	@GET
	@Path("/sendOrders")
	@Produces(MediaType.TEXT_HTML)
	public Viewable sendOrders(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("IS_AUTHENTICATED") != null
				&& session.getAttribute("IS_AUTHENTICATED").equals("TRUE")) {
			Map<String, String> map = new HashMap<>();
			map.put("page", "sendOrders");
			map.put("authStatus", (String) session.getAttribute("IS_AUTHENTICATED"));
			return new Viewable("/template.ftl", map);
		}
		return null;
	}

	@GET
	@Path("/lastOrders")
	@Produces(MediaType.TEXT_HTML)
	public Viewable getLastOrders(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("IS_AUTHENTICATED") != null
				&& session.getAttribute("IS_AUTHENTICATED").equals("TRUE")) {
			Map<String, String> map = new HashMap<>();
			map.put("page", "lastOrders");
			map.put("authStatus", (String) session.getAttribute("IS_AUTHENTICATED"));
			return new Viewable("/template.ftl", map);
		}
		return null;
	}

	@GET
	@Path("/contact")
	@Produces(MediaType.TEXT_HTML)
	public Viewable getContact(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		Map<String, String> map = new HashMap<>();
		map.put("page", "contact");
		if (session.getAttribute("IS_AUTHENTICATED") != null) {
			map.put("authStatus", (String) session.getAttribute("IS_AUTHENTICATED"));
		} else {
			map.put("authStatus", "FALSE");
		}

		return new Viewable("/template.ftl", map);

	}

}
