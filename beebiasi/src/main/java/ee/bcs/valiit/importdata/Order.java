package ee.bcs.valiit.importdata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

	private String paymentTime;
	private String docDate;
	private int documentNum; // the same as Order ID
	private String info;
	private float amount;
	private float amountWithoutDelivery;
	private List<OrderItem> orderListOfOrderItems = new ArrayList<OrderItem>();

	public Order() {
		this.info = "";
		this.docDate = getToday();
		this.paymentTime = getToday();
	}

	public String getToday() {
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date today = new Date();
		return dateFormat.format(today);
	}

	public String getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(String paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getDocDate() {
		return docDate;
	}

	public void setDocDate(String docDate) {
		this.docDate = docDate;
	}

	public int getDocumentNum() {
		return documentNum;
	}

	public void setDocumentNum(int documentNum) {
		this.documentNum = documentNum;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public float getAmountWithoutDelivery() {
		return amountWithoutDelivery;
	}

	public void setAmountWithoutDelivery(float amountWithoutDelivery) {
		this.amountWithoutDelivery = amountWithoutDelivery;
	}

	public List<OrderItem> getOrderListOfOrderItems() {
		return orderListOfOrderItems;
	}

	public void setOrderListOfOrderItems(List<OrderItem> orderListOfOrderItems) {
		this.orderListOfOrderItems = orderListOfOrderItems;

	}

}
