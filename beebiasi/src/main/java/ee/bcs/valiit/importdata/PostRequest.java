package ee.bcs.valiit.importdata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class PostRequest {

	public static String sendPostRequest() throws ClientProtocolException, IOException {

		String info = postJson();
		
		info += "<br>" + postXml("fiBox");
		info += "<br>" + postXml("kuller");
		info += "<br>" + postXml("eeBox");
		return info;

	}


	public static String postJson() throws ClientProtocolException, IOException {

		List<Order> orderList = ReadFromExcel.getOrderList();
		String bearer = Authorisation.getBearer();
		String url = "https://www.compucash5.com/ccapi/InvoicesA4/ImportInvoice";
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		post.setHeader("Authorization", bearer);

		String responseInfo = "";
		Integer orderCount = 0;
		for (Order order : orderList) {
			String jsonExport = OrderExport.getJson(order);
			StringEntity entity = new StringEntity(jsonExport, ContentType.APPLICATION_JSON);
			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";

			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			if (response.getStatusLine().getStatusCode() == 200) {
				orderCount++;
			} else if (response.getStatusLine().getStatusCode() == 401) {
				responseInfo += "Authentication denied! ";
			} else {
				responseInfo += "Orders loading to CompuCash failed. Responce: "
						+ response.getStatusLine().getStatusCode() + "; ";
			}
		}

		switch (orderCount) {
		case 0:
			break;
		case 1:
			responseInfo += "Order loaded to CompuCash! ";
			break;
		default:
			responseInfo += orderCount.toString() + " orders loaded to CompuCash! ";
		}

		return responseInfo;
	}

	public static String postXml(String postType) throws ClientProtocolException, IOException {
		String xml = ReadSmartPostInfo.createOrderXML(postType);
		String responseInfo = "";
		String postTypeName = "";
		if (postType.equals("fiBox"))
			postTypeName = "SmartPost FI";
		else if (postType.equals("kuller"))
			postTypeName = "Smart Kuller";
		else if (postType.equals("eeBox"))
			postTypeName = "SmartPost EE";

		if (!xml.equals("")) {
			String url = "http://iseteenindus.smartpost.ee/api/?request=shipment";
			HttpClient client = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(url);
			StringEntity entity = new StringEntity(xml, ContentType.APPLICATION_XML.withCharset(Charset.forName("utf-8")));
			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";

			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			if (response.getStatusLine().getStatusCode() == 200) {

				responseInfo += postTypeName + " export successful!";
			} else {
				responseInfo += postTypeName + " export failed. Responce: " + response.getStatusLine().getStatusCode()
						+ "; ";
			}
		}

		return responseInfo;

	}
}
