package ee.bcs.valiit.importdata;

import java.math.BigInteger;

public class OrderItem {

	private BigInteger barcode;
	private String description;
	private int quantity;
	private float salePrice;
	private int fkOrderId;
	private String orderItemId;
	private String orderItemInfo;

	public OrderItem() {

	}

	public BigInteger getBarcode() {
		return barcode;
	}

	public void setBarcode(BigInteger barcode) {
		this.barcode = barcode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}

	public int getFkOrderId() {
		return fkOrderId;
	}

	public void setFkOrderId(int fkOrderId) {
		this.fkOrderId = fkOrderId;
	}

	public String getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(String orderItemId) {
		this.orderItemId = orderItemId;
	}

	public String getOrderItemInfo() {
		return orderItemInfo;
	}

	public void setOrderItemInfo(String orderItemInfo) {
		this.orderItemInfo = orderItemInfo;
	}

}