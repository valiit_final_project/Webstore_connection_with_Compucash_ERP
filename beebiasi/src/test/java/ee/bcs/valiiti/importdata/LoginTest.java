package ee.bcs.valiiti.importdata;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.valiit.importdata.Login;

public class LoginTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void loginTest() {
		String login = Login.login("admin", "hello");
		assertTrue(login.equals("Welcome!"));
		
		login = Login.login("test", "test");
		assertTrue(login.equals("Please try again!"));
		
		login = Login.login("test", "hello");
		assertTrue(login.equals("Please try again!"));
		
		login = Login.login("admin", "test");
		assertTrue(login.equals("Please try again!"));
	}

}
