package ee.bcs.valiiti.importdata;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.bcs.valiit.importdata.Order;
import ee.bcs.valiit.importdata.OrdersFromDatabase;
import ee.bcs.valiit.importdata.ReadFromExcel;

public class ReadFromExcelTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void readFromExcelTest() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		
	}
	@Test
	public void readFromExcelTest1() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test1.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		
	}
	@Test
	public void readFromExcelTest2() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test2.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		
	}
	@Test
	public void readFromExcelTest3() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test3.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		
	}
	@Test
	public void readFromExcelTest0() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test0.xlsx");
		assertFalse(test.contains("Excel reading successful!"));
		
	}
	
	@Test
	public void readFromExcelTest5() {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test5.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		
	}
	
	@Test
	public void readFromExcelTest4() throws ClassNotFoundException, SQLException {
				
		String test = ReadFromExcel.readFromExcel(".\\order_informaton_test4.xlsx");
		assertTrue(test.contains("Excel reading successful!"));
		List<Order> dbOrderList = OrdersFromDatabase.getDBOrderList();
		assertFalse(dbOrderList.isEmpty());
	}


	


}
