package ee.bcs.valiit.importdata;


public class SmartPostItem {

	private String reference;
	private SmartPostRecipient recipient;
	private SmartPostDestination destination;
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public SmartPostRecipient getRecipient() {
		return recipient;
	}
	public void setRecipient(SmartPostRecipient recipient) {
		this.recipient = recipient;
	}
	public SmartPostDestination getDestination() {
		return destination;
	}
	public void setDestination(SmartPostDestination destination) {
		this.destination = destination;
	}

		
}
