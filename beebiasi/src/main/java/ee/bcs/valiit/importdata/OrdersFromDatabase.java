package ee.bcs.valiit.importdata;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrdersFromDatabase {

	private final static String CONNECTION_URL = "jdbc:mysql://localhost:3306/beebiasi";
	private final static String CONNECTION_USERNAME = "root";
	private final static String CONNECTION_PASSWORD = "tere";


	
	public static ResultSet performSqlSelect(String sql) throws ClassNotFoundException, SQLException {

		Connection conn = null;
		Statement stmt = null;

		Class.forName("org.mariadb.jdbc.Driver");
		conn = DriverManager.getConnection(CONNECTION_URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
		stmt = conn.createStatement();

		ResultSet resultSet = stmt.executeQuery(sql);

		if (conn != null) {
			conn.close();
		}
		return resultSet;
	}

	public static List<Order> getDBOrderList() throws ClassNotFoundException, SQLException {
		List<OrderItem> dbOrderItemList = new ArrayList<>();
		ResultSet result = OrdersFromDatabase.performSqlSelect("select *  from beebiasi.order_item;");
		while (result.next()) {
			OrderItem orderItem = new OrderItem();
			orderItem.setBarcode(BigInteger.valueOf(Long.parseLong(result.getString(2))));
			orderItem.setDescription(result.getString(3));
			orderItem.setFkOrderId(Integer.parseInt(result.getString(4)));
			orderItem.setQuantity(Integer.parseInt(result.getString(5)));
			orderItem.setSalePrice(Float.parseFloat(result.getString(6)));

			dbOrderItemList.add(orderItem);
		}
		
		List<Order> dbOrderList = new ArrayList<>();
		result = OrdersFromDatabase.performSqlSelect("select id, amount, info from beebiasi.order;");
		while (result.next()) {
			Order order = new Order();
			order.setDocumentNum(Integer.parseInt(result.getString(1)));
			order.setAmount(Float.parseFloat(result.getString(2)));
			order.setInfo(result.getString(3));
			dbOrderList.add(order);
		}
		
		for (Order order: dbOrderList) {
			for(OrderItem orderItem: dbOrderItemList ) {
				if (orderItem.getFkOrderId() == order.getDocumentNum()){
					order.getOrderListOfOrderItems().add(orderItem);
				}
			}
			
		}
		
		return dbOrderList;
	}

}
