package ee.bcs.valiit.importdata;

public class JsonDocumentInfoTO {

	public String docDate;
	public String documentNum;
	public String info;

	public JsonDocumentInfoTO(Order order) {
		this.docDate = order.getDocDate();
		this.documentNum = String.valueOf(order.getDocumentNum());
		this.info = order.getInfo();
	}



}
