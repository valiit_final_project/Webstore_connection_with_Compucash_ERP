package ee.bcs.valiit.importdata;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImportData {
	private static final int BUFFER_SIZE = 4096;

	public static String downloadFile(String fileURL, String saveDir) throws IOException {
				
		URL url = new URL(fileURL);
		
		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		String myCookie = "SSESSc86038e21c30581e27c0f64e6a22592c=UMc_wfKO-kL4ZVlGJbaLW4NGm3hbCKdhX95j_DUKCng";
		httpConn.setRequestProperty("Cookie", myCookie);
		httpConn.connect();
		
		int responseCode = httpConn.getResponseCode();

		if (responseCode == HttpURLConnection.HTTP_OK) {
			InputStream inputStream = httpConn.getInputStream();
			String saveFilePath = saveDir + File.separator + "order_information.xlsx";
			FileOutputStream outputStream = new FileOutputStream(saveFilePath);
			int bytesRead = -1;
			byte[] buffer = new byte[BUFFER_SIZE];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();
			httpConn.disconnect();
			return "File downloaded!\r\n ";
		} else {
			httpConn.disconnect();
			return "No file to download. Server replied HTTP code: " + responseCode +"\r\n";
		
		}
		
	}
}